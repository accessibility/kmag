# translation of kmag.po to Dutch
# translation of kmag.po to
# translation of kmag.po to
# Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009 Free Software Foundation, Inc.
# Proefgelezen 03-10-2003 Paul Baardman <paul.baardman@euronet.nl>
# Rinse de Vries <rinse@kde.nl>, 2003, 2004.
# Rinse de Vries <rinsedevries@kde.nl>, 2004, 2005, 2007, 2008, 2009.
# Bram Schoenmakers <bramschoenmakers@kde.nl>, 2004, 2005, 2006.
# Rinse de Vries <RinseDeVries@home.nl>, 2005.
# Freek de Kruijf <f.de.kruijf@hetnet.nl>, 2009.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2010, 2011, 2012, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kmag\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-08 01:33+0000\n"
"PO-Revision-Date: 2017-07-17 10:29+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Rinse de Vries,Freek de Kruijf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "rinsedevries@kde.nl,freekdekruijf@kde.nl"

# nagelezen op 1-11-2009
#: kmag.cpp:52
#, kde-format
msgctxt "Zoom at very low"
msgid "&Very Low"
msgstr "&Zeer laag"

#: kmag.cpp:52
#, kde-format
msgctxt "Zoom at low"
msgid "&Low"
msgstr "&Laag"

#: kmag.cpp:52
#, kde-format
msgctxt "Zoom at medium"
msgid "&Medium"
msgstr "&Middel"

#: kmag.cpp:52
#, kde-format
msgctxt "Zoom at high"
msgid "&High"
msgstr "&Hoog"

#: kmag.cpp:52
#, kde-format
msgctxt "Zoom at very high"
msgid "V&ery High"
msgstr "Z&eer hoog"

#: kmag.cpp:60
#, kde-format
msgctxt "No color-blindness simulation, i.e. 'normal' vision"
msgid "&Normal"
msgstr "&Normaal"

#: kmag.cpp:60
#, kde-format
msgid "&Protanopia"
msgstr "&Protanomalie"

#: kmag.cpp:60
#, kde-format
msgid "&Deuteranopia"
msgstr "&Deuteranomalie"

#: kmag.cpp:60
#, kde-format
msgid "&Tritanopia"
msgstr "&Tritanomalie"

#: kmag.cpp:60
#, kde-format
msgid "&Achromatopsia"
msgstr "&Achromatopsie"

#: kmag.cpp:68
#, kde-format
msgid "&No Rotation (0 Degrees)"
msgstr "Gee&n rotatie (0 graden)"

#: kmag.cpp:68
#, kde-format
msgid "&Left (90 Degrees)"
msgstr "&Links (90 graden)"

#: kmag.cpp:68
#, kde-format
msgid "&Upside Down (180 Degrees)"
msgstr "&Ondersteboven (180 graden)"

#: kmag.cpp:68
#, kde-format
msgid "&Right (270 Degrees)"
msgstr "&Rechts (270 graden)"

#: kmag.cpp:100
#, kde-format
msgid "New &Window"
msgstr "Nieuw &venster"

#: kmag.cpp:103
#, kde-format
msgid "Open a new KMagnifier window"
msgstr "Open een nieuw KMagnifier-venster"

#: kmag.cpp:107
#, kde-format
msgid "&Stop"
msgstr "&Stoppen"

#: kmag.cpp:110
#, kde-format
msgid "Click to stop window refresh"
msgstr "Klik om het verversen van het venster te stoppen"

#: kmag.cpp:111
#, kde-format
msgid ""
"Clicking on this icon will <b>start</b> / <b>stop</b> updating of the "
"display. Stopping the update will zero the processing power required (CPU "
"usage)"
msgstr ""
"Door op dit pictogram te klikken kunt u het bijwerken van het scherm "
"<b>starten</b> of <b>stoppen</b>. Bij het stoppen van het bijwerken zal het "
"vereiste processorgebruik terug gaan naar nul."

#: kmag.cpp:117
#, kde-format
msgid "&Save Snapshot As..."
msgstr "Schermafdruk ops&laan als..."

#: kmag.cpp:120
#, kde-format
msgid "Saves the zoomed view to an image file."
msgstr "Bewaart de vergrote weergave in een afbeeldingsbestand."

#: kmag.cpp:121
#, kde-format
msgid "Save image to a file"
msgstr "Afbeelding opslaan naar bestand"

#: kmag.cpp:124
#, kde-format
msgid "Click on this button to print the current zoomed view."
msgstr "Klik op deze knop om de huidige vergrote weergave af te drukken."

#: kmag.cpp:127 kmag.cpp:128
#, kde-format
msgid "Quits the application"
msgstr "Sluit de toepassing af"

#: kmag.cpp:131
#, kde-format
msgid ""
"Click on this button to copy the current zoomed view to the clipboard which "
"you can paste in other applications."
msgstr ""
"Klik op deze knop om de huidige vergrote weergave naar het klembord te "
"kopiëren zodat u het in andere toepassingen kunt plakken."

#: kmag.cpp:132
#, kde-format
msgid "Copy zoomed image to clipboard"
msgstr "Vergrote afbeelding kopiëren naar klembord"

#: kmag.cpp:138
#, kde-format
msgid "&Follow Mouse Mode"
msgstr "Muis-&volgen-modus"

#: kmag.cpp:142
#, kde-format
msgid "Mouse"
msgstr "Muis"

#: kmag.cpp:143
#, kde-format
msgid "Magnify around the mouse cursor"
msgstr "Vergroten rond de muisaanwijzer"

#: kmag.cpp:144
#, kde-format
msgid "If selected, the area around the mouse cursor is magnified"
msgstr ""
"Indien ingeschakeld zal het gebied rond de muisaanwijzer worden uitvergroot"

#: kmag.cpp:148
#, kde-format
msgid "&Follow Focus Mode"
msgstr "Focus-&volgen-modus"

#: kmag.cpp:152
#, kde-format
msgid "Focus"
msgstr "Focus"

#: kmag.cpp:153
#, kde-format
msgid "Magnify around the keyboard focus"
msgstr "Vergroten rond de toetesenbordfocus"

#: kmag.cpp:154
#, kde-format
msgid "If selected, the area around the keyboard cursor is magnified"
msgstr ""
"Indien ingeschakeld zal het gebied rond de toetsenbordcursor worden vergroot"

#: kmag.cpp:158
#, kde-format
msgid "Se&lection Window Mode"
msgstr "Se&lectievenster activeren"

#: kmag.cpp:162
#, kde-format
msgid "Window"
msgstr "Venster"

#: kmag.cpp:163
#, kde-format
msgid "Show a window for selecting the magnified area"
msgstr "Toon een venster voor het selecteren van het uitvergrote gebied"

#: kmag.cpp:165
#, kde-format
msgid "&Whole Screen Mode"
msgstr "&Volledig scherm activeren"

#: kmag.cpp:169
#, kde-format
msgid "Screen"
msgstr "Scherm"

#: kmag.cpp:170
#, kde-format
msgid "Magnify the whole screen"
msgstr "Vergroot het hele scherm"

#: kmag.cpp:171
#, kde-format
msgid "Click on this button to fit the zoom view to the zoom window."
msgstr "Klik op deze knop om het zoomgebied aan te passen aan het zoomvenster."

#: kmag.cpp:173
#, kde-format
msgid "Hide Mouse &Cursor"
msgstr "Muisaanwij&zer verbergen"

#: kmag.cpp:178
#, kde-format
msgid "Show Mouse &Cursor"
msgstr "Muisaanwij&zer tonen"

#: kmag.cpp:180
#, kde-format
msgid "Hide"
msgstr "Verbergen"

#: kmag.cpp:181
#, kde-format
msgid "Hide the mouse cursor"
msgstr "Verberg de muisaanwijzer"

#: kmag.cpp:183
#, kde-format
msgid "Stays On Top"
msgstr "Altijd op voorgrond"

#: kmag.cpp:187
#, kde-format
msgid "The KMagnifier Window stays on top of other windows."
msgstr "Het venster van KMagnifier blijft boven andere vensters."

#: kmag.cpp:190
#, kde-format
msgid "Click on this button to <b>zoom-in</b> on the selected region."
msgstr "Klik op deze knop om de geselecteerde regio te <b>vergroten</b>."

#: kmag.cpp:192
#, kde-format
msgid "&Zoom"
msgstr "&Zoomen"

#: kmag.cpp:195
#, kde-format
msgid "Select the zoom factor."
msgstr "Selecteer de zoomfactor."

#: kmag.cpp:196
#, kde-format
msgid "Zoom factor"
msgstr "Zoomfactor"

#: kmag.cpp:199
#, kde-format
msgid "Click on this button to <b>zoom-out</b> on the selected region."
msgstr "Klik op deze knop om het geselecteerde gebied te <b>verkleinen</b>."

#: kmag.cpp:201
#, kde-format
msgid "&Rotation"
msgstr "&Rotatie"

#: kmag.cpp:204
#, kde-format
msgid "Select the rotation degree."
msgstr "Selecteer de rotatiehoek."

#: kmag.cpp:205
#, kde-format
msgid "Rotation degree"
msgstr "Rotatiehoek"

#: kmag.cpp:210
#, kde-format
msgid "&Refresh"
msgstr "Ve&rnieuwen"

#: kmag.cpp:213
#, kde-format
msgid ""
"Select the refresh rate. The higher the rate, the more computing power (CPU) "
"will be needed."
msgstr ""
"Selecteer de vernieuwingsfrequentie. Hoe hoger de frequentie, des te meer "
"processorkracht er nodig zal zijn."

#: kmag.cpp:214
#, kde-format
msgid "Refresh rate"
msgstr "Vernieuwingsfrequentie"

#: kmag.cpp:216
#, kde-format
msgctxt "Color-blindness simulation mode"
msgid "&Color"
msgstr "&Kleur"

#: kmag.cpp:219
#, kde-format
msgid "Select a mode to simulate various types of color-blindness."
msgstr "Selecteer een modus om diverse typen kleurenblindheid te simuleren."

#: kmag.cpp:220
#, kde-format
msgid "Color-blindness Simulation Mode"
msgstr "Simulatie van kleurenblindheid"

#: kmag.cpp:552
#, kde-format
msgid "Save Snapshot As"
msgstr "Schermafdruk opslaan als"

#: kmag.cpp:566
#, kde-format
msgid ""
"Unable to save temporary file (before uploading to the network file you "
"specified)."
msgstr ""
"Het tijdelijke bestand kon niet worden opgeslagen (voordat het wordt "
"verzonden naar het netwerkbestand dat u opgaf)."

#: kmag.cpp:567 kmag.cpp:572 kmag.cpp:582
#, kde-format
msgid "Error Writing File"
msgstr "Fout bij opslaan van bestand"

#: kmag.cpp:571
#, kde-format
msgid "Unable to upload file over the network."
msgstr "Het bestand kon niet over het netwerk worden verzonden."

#: kmag.cpp:574 kmag.cpp:584
#, kde-format
msgid ""
"Current zoomed image saved to\n"
"%1"
msgstr ""
"Huidige vergrote afbeelding opgeslagen naar\n"
"%1"

#: kmag.cpp:575 kmag.cpp:585
#, kde-format
msgid "Information"
msgstr "Informatie"

#: kmag.cpp:581
#, kde-format
msgid ""
"Unable to save file. Please check if you have permission to write to the "
"directory."
msgstr ""
"Het bestand kon niet worden opgeslagen. Controleer a.u.b. of u de benodigde "
"toegangsrechten hebt om naar de map te schrijven."

#: kmag.cpp:600
#, kde-format
msgid "Stop"
msgstr "Stoppen"

#: kmag.cpp:601
#, kde-format
msgid "Click to stop window update"
msgstr "Klik om het bijwerken van het venster te stoppen"

#: kmag.cpp:604
#, kde-format
msgctxt "Start updating the window"
msgid "Start"
msgstr "Starten"

#: kmag.cpp:605
#, kde-format
msgid "Click to start window update"
msgstr "Klik om het bijwerken van het venster te starten"

#: kmagselrect.cpp:198
#, kde-format
msgid "Selection Window"
msgstr "Selectievenster"

#: kmagselrect.cpp:198 main.cpp:38
#, kde-format
msgid "KMagnifier"
msgstr "KMagnifier"

#. i18n: ectx: ToolBar (mainToolBar)
#: kmagui.rc:3
#, kde-format
msgid "Main Toolbar"
msgstr "Hoofdwerkbalk"

#. i18n: ectx: ToolBar (viewToolBar)
#: kmagui.rc:6
#, kde-format
msgid "View Toolbar"
msgstr "Weergavebalk"

#. i18n: ectx: ToolBar (settingsToolBar)
#: kmagui.rc:16
#, kde-format
msgid "Settings Toolbar"
msgstr "Instellingenbalk"

#. i18n: ectx: Menu (file)
#: kmagui.rc:25
#, kde-format
msgid "&File"
msgstr "&Bestand"

#. i18n: ectx: Menu (edit)
#: kmagui.rc:32
#, kde-format
msgid "&Edit"
msgstr "Be&werken"

#. i18n: ectx: Menu (view)
#: kmagui.rc:35 kmagui.rc:73
#, kde-format
msgid "&View"
msgstr "&Beeld"

#. i18n: ectx: Menu (settings)
#: kmagui.rc:47 kmagui.rc:83
#, kde-format
msgid "&Settings"
msgstr "&Instellingen"

#: kmagzoomview.cpp:107
#, kde-format
msgid ""
"This is the main window which shows the contents of the selected region. The "
"contents will be magnified according to the zoom level that is set."
msgstr ""
"Dit is het hoofdvenster dat de inhoud van het geselecteerde gebied zal "
"weergeven. De inhoud zal worden vergroot volgens het ingestelde zoomniveau."

#: main.cpp:39
#, kde-format
msgid "Screen magnifier created by KDE"
msgstr "Schermvergrootglas door KDE"

#: main.cpp:41
#, kde-format
msgid ""
"Copyright 2001-2003 Sarang Lakare\n"
"Copyright 2003-2004 Olaf Schmidt\n"
"Copyright 2008 Matthew Woehlke"
msgstr ""
"Copyright 2001-2003 Sarang Lakare\n"
"Copyright 2003-2004 Olaf Schmidt\n"
"Copyright 2008 Matthew Woehlke"

#: main.cpp:45
#, kde-format
msgid "Sarang Lakare"
msgstr "Sarang Lakare"

#: main.cpp:46
#, kde-format
msgid "Rewrite"
msgstr "Herschrijving"

#: main.cpp:47
#, kde-format
msgid "Michael Forster"
msgstr "Michael Forster"

#: main.cpp:48
#, kde-format
msgid "Original idea and author (KDE1)"
msgstr "Oorspronkelijk idee en auteur (KDE1)"

#: main.cpp:49
#, kde-format
msgid "Olaf Schmidt"
msgstr "Olaf Schmidt"

#: main.cpp:49
#, kde-format
msgid ""
"Rework of the user interface, improved selection window, speed optimization, "
"rotation, bug fixes"
msgstr ""
"Verbeteringen gebruikersinterface, beter selectievenster, optimalisaties, "
"rotatie en fouten opgelost"

#: main.cpp:50
#, kde-format
msgid "Matthew Woehlke"
msgstr "Matthew Woehlke"

#: main.cpp:50
#, kde-format
msgid "Color-blindness simulation"
msgstr "Kleurenblindheidsimulatie"

#: main.cpp:51
#, kde-format
msgid "Sebastian Sauer"
msgstr "Sebastian Sauer"

#: main.cpp:51
#, kde-format
msgid "Focus tracking"
msgstr "Volgen van focus"

#: main.cpp:52
#, kde-format
msgid "Claudiu Costin"
msgstr "Claudiu Costin"

#: main.cpp:52
#, kde-format
msgid "Some tips"
msgstr "Enkele tips"

#~ msgid "File to open"
#~ msgstr "Bestand om te openen"

#~ msgid "Show &Menu"
#~ msgstr "&Menu tonen"

#~ msgid "Hide &Menu"
#~ msgstr "&Menu verbergen"

#~ msgid "Hide Main &Toolbar"
#~ msgstr "Hoofdwerkbalk ve&rbergen"

#~ msgid "Hide &View Toolbar"
#~ msgstr "&Weergavebalk verbergen"

#~ msgid "Hide &Settings Toolbar"
#~ msgstr "In&stellingenbalk verbergen"
